#include "Chili/Tools/MPI.hh"
#include "spdlog/spdlog.h"

using chili::MPIHandler;

MPIHandler MPIHandler::mpi;

void MPIHandler::Init(int argc, char* argv[]) {
#ifdef CHILI_ENABLE_MPI
    MPI_Init(&argc, &argv);
    mComm = MPI_COMM_WORLD;
    InitTypes();
#else
    (void)argc;
    (void)argv;
#endif
}

void MPIHandler::InitTypes() {
#ifdef CHILI_ENABLE_MPI
    MPI_Datatype stats_type, double_type;
    MPI_Type_contiguous(6, MPI_DOUBLE, &stats_type);
    MPI_Type_contiguous(1, MPI_DOUBLE, &double_type);
    mpi.RegisterType<chili::StatsData>(stats_type);
    mpi.RegisterType<double>(double_type);
    mpi.RegisterOp<chili::StatsAdd>();
    mpi.RegisterOp<chili::Add>();
#endif
}

void MPIHandler::PrintRankInfo() {
#ifdef CHILI_ENABLE_MPI
    const int size = Size();
    if(size > 1)
        spdlog::info("Running on {} ranks.", size);
#endif
}
