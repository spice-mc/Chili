#pragma once

#include "Chili/Channel/ChannelUtils.hh"
#include <cmath>
#include <map>
#include <unordered_map>
#include <vector>

#include <iostream>

namespace chili {

struct Cuts {
    std::vector<double> sexternal;
    std::map<std::pair<int, int>, double> deltaR;
    std::unordered_map<unsigned int, double> smin;
    std::unordered_map<unsigned int, double> ptmin;
    std::unordered_map<unsigned int, double> etamax;

    bool Deserialize(std::istream &in) {
        size_t size;
        in.read(reinterpret_cast<char*>(&size), sizeof(size));
        sexternal.resize(size);
        for(auto &mass : sexternal) {
            in.read(reinterpret_cast<char*>(&mass), sizeof(mass));
        }

        // Read in deltaR map
        in.read(reinterpret_cast<char*>(&size), sizeof(size));
        for(size_t ielm = 0; ielm < size; ++ielm) {
            int pid1, pid2;
            double dR;
            in.read(reinterpret_cast<char*>(&pid1), sizeof(pid1));
            in.read(reinterpret_cast<char*>(&pid2), sizeof(pid2));
            in.read(reinterpret_cast<char*>(&dR), sizeof(dR));
            deltaR[{pid1, pid2}] = dR;
        }

        // Read in smin map
        in.read(reinterpret_cast<char*>(&size), sizeof(size));
        for(size_t ielm = 0; ielm < size; ++ielm) {
            unsigned int idx;
            double val;
            in.read(reinterpret_cast<char*>(&idx), sizeof(idx));
            in.read(reinterpret_cast<char*>(&val), sizeof(val));
            smin[idx] = val;
        }

        // Read in ptmin map
        in.read(reinterpret_cast<char*>(&size), sizeof(size));
        for(size_t ielm = 0; ielm < size; ++ielm) {
            unsigned int idx;
            double val;
            in.read(reinterpret_cast<char*>(&idx), sizeof(idx));
            in.read(reinterpret_cast<char*>(&val), sizeof(val));
            ptmin[idx] = val;
        }

        // Read in etamax map
        in.read(reinterpret_cast<char*>(&size), sizeof(size));
        for(size_t ielm = 0; ielm < size; ++ielm) {
            unsigned int idx;
            double val;
            in.read(reinterpret_cast<char*>(&idx), sizeof(idx));
            in.read(reinterpret_cast<char*>(&val), sizeof(val));
            etamax[idx] = val;
        }
        return true;
    }

    bool Serialize(std::ostream &out) const {
        size_t size = sexternal.size();
        out.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
        for(auto &mass : sexternal) {
            out.write(reinterpret_cast<const char*>(&mass), sizeof(mass));
        }

        // Write out deltaR map
        size = deltaR.size();
        out.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
        for(const auto &elm : deltaR) {
            out.write(reinterpret_cast<const char*>(&elm.first.first), sizeof(int));
            out.write(reinterpret_cast<const char*>(&elm.first.second), sizeof(int));
            out.write(reinterpret_cast<const char*>(&elm.second), sizeof(double));
        }

        // Write out smin map
        size = smin.size();
        out.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
        for(const auto &sval : smin) {
            out.write(reinterpret_cast<const char*>(&sval.first), sizeof(unsigned int));
            out.write(reinterpret_cast<const char*>(&sval.second), sizeof(double));
        }

        // Write out ptmin map
        size = ptmin.size();
        out.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
        for(const auto &ptval : ptmin) {
            out.write(reinterpret_cast<const char*>(&ptval.first), sizeof(unsigned int));
            out.write(reinterpret_cast<const char*>(&ptval.second), sizeof(double));
        }

        // Write out etamax map
        size = etamax.size();
        out.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
        for(const auto &etaval : etamax) {
            out.write(reinterpret_cast<const char*>(&etaval.first), sizeof(unsigned int));
            out.write(reinterpret_cast<const char*>(&etaval.second), sizeof(double));
        }
        return true;
    }
};

inline double SMax(double sqrts, const Cuts &cuts, unsigned int idx) {
    double result = sqrts;
    for(unsigned int i = 0; i < cuts.sexternal.size(); ++i) {
        if(!BitIsSet(idx, i+2)) {
            result -= sqrt(cuts.sexternal[i]);
        }
    }
    return result*result;
}

}
